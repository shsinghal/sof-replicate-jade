const express = require('express')
const bodyParser = require('body-parser');
const app = express()
const nodemailer = require("nodemailer");
app.set('view engine','jade');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));


app.post('/formProcess', function (req, res) {
    const data=`
    <p> you have a new contact request</p>
    <h3>Contact Details</h3>
    <li>First Name: $(req.body.fname)</li>
    <li>Last Name: $(req.body.lname)</li>
    <li>Email Address: $(req.body.email)</li>
    <li>Contact Number : $(req.body.contact)</li>
    <li>Designation: $(req.body.designation)</li>
    <li>Message: $(req.body.message)</li>`
;
    var smtpTransport = nodemailer.createTransport("SMTP",{
       service: "Gmail", 
       auth: {
       user: "shsinghal1997@gmail.com",
       pass: ""
       }
            
   });

   smtpTransport.sendMail({  //email options
   from: "shsinghal1997@gmail.com",
   to: "shsinghal1997@gmail.com", // receiver
   subject: "Emailing with nodemailer", // subject
   html: "data" // body (var data which we've declared)
    }, function(error, response){  //callback
         if(error){
           console.log(error);
        }else{
           console.log("Message sent: " + res.message);
       }

   smtpTransport.close(); 
    }); });

app.get('/' , function (req,res) {
  res.render('home')
})

app.get('/home',function(req,res) {
 res.render('home');
})

app.get('/about-us',function(req,res){ 
      res.render('about');
    })
app.get('/community' , function(req,res){
    res.render('community');
})

app.get('/blog' , function(req,res){
    res.redirect('https://medium.com/@aditiagrawal_49691');
})

app.get('/contact',function(req,res)
{
  res.render('contact');
})
app.get('/be-a-trailblazer',function(req,res)
{
  res.render('be-a-trailblazer');
})


app.listen(7000,function(){
  console.log('Magic happens on port 7000');
})